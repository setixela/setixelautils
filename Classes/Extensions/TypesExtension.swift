//
//  TypesExtension.swift
//  Invercity
//
//  Created by AlexSolo on 09.03.17.
//  Copyright © 2017 Duoyulong. All rights reserved.
//

import Foundation

public extension Bool {
   public init<T: BinaryInteger>(_ integer: T) {
      self.init(integer != 0)
   }
}

public extension Int {
   public func format(_ format: String) -> String {
      return NSString(format: "%\(format)d" as NSString, self) as String
   }

   public func isBitSet(bit: Int) -> Bool {
      if ((self >> (bit - 1)) & 1) >= 1 {
         return true
      } else {
         return false
      }
   }
}

public extension Double {
   public func format(_ format: String) -> String {
      return NSString(format: "%\(format)f" as NSString, self) as String
   }
}

public extension CGFloat {
   public static func random() -> CGFloat {
      return CGFloat(arc4random()) / CGFloat(UInt32.max)
   }

   public func subdivide(width: CGFloat, count: Int) -> [CGFloat] {
      let countFloat = CGFloat(count - 1)
      let step = (self - width) / countFloat

      var points: [CGFloat] = []

      for index in 0..<count {
         let point = CGFloat(index) * step
         points.append(point)
      }

      return points
   }
}

public extension Int {
   public static func random(max: Int) -> Int {
      return Int(arc4random_uniform(UInt32(max)))
   }
}

public extension Int {
   public var stringValue: String {
      return String(self)
   }
}
