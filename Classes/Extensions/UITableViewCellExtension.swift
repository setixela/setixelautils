//
//  UITableViewCellExtension.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 14.02.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public extension UITableViewCell {
   public  var tableView: UITableView? {
      var view = self.superview
      
      while view != nil && view!.isKind(of: UITableView.self) == false {
         view = view!.superview
      }
      return view as? UITableView
   }
}
