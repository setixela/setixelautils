//
//  BinaryTree.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 12/11/2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public indirect enum BinaryTree<T> {
   case node(BinaryTree<T>, T, BinaryTree<T>)
   case empty
}

extension BinaryTree {
   public var count: Int {
      switch self {
      case let .node(left, _, right):
         return left.count + 1 + right.count
      case .empty:
         return 0
      }
   }

   public func traverseInOrder(process: (T) -> Void) {
      if case let .node(left, value, right) = self {
         left.traverseInOrder(process: process)
         process(value)
         right.traverseInOrder(process: process)
      }
   }

   public func traversePreOrder(process: (T) -> Void) {
      if case let .node(left, value, right) = self {
         process(value)
         left.traversePreOrder(process: process)
         right.traversePreOrder(process: process)
      }
   }

   public func traversePostOrder(process: (T) -> Void) {
      if case let .node(left, value, right) = self {
         left.traversePostOrder(process: process)
         right.traversePostOrder(process: process)
         process(value)
      }
   }
}

extension BinaryTree: CustomStringConvertible {
   public var description: String {
      switch self {
      case let .node(left, value, right):
         return "value: \(value), \n left = [\(left.description)], \n right = [\(right.description)]"
      case .empty:
         return ""
      }
   }
}
