//
//  UITextViewExtension.swift
//  SetixelaUtils
//
//  Created by AlexSolo on 22.12.2017.
//  Copyright © 2017 XYZ Project. All rights reserved.
//

import Foundation

public extension UITextView {
   func boundingRectForCharacterRange(range: NSRange) -> CGRect? {
      guard let attributedText = attributedText else { return nil }

      let textStorage = NSTextStorage(attributedString: attributedText)
      let layoutManager = NSLayoutManager()
      
      textStorage.addLayoutManager(layoutManager)
      
      let textContainer = NSTextContainer(size: self.contentSize)
      textContainer.lineFragmentPadding = 0.0
      
      layoutManager.addTextContainer(textContainer)
      
      var glyphRange = NSRange()
      
      // Convert the range for glyphs.
      layoutManager.characterRange(forGlyphRange: range, actualGlyphRange: &glyphRange)
      
      return layoutManager.boundingRect(forGlyphRange: glyphRange, in: textContainer)
   }
}
