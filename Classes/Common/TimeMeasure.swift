//
//  TimeMeasure.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 03.05.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

fileprivate struct TimeMeasureData: Equatable {
   static func == (lhs: TimeMeasureData, rhs: TimeMeasureData) -> Bool {
      return lhs.name == rhs.name
   }
   
   var startTime: DispatchTime
   var name: String
}

class TimeMeasure {
   
   fileprivate static var measures = [TimeMeasureData]()
   
   private class func printResult(measure: TimeMeasureData) {
      
      let endTime = DispatchTime.now()
      let nanoTime = endTime.uptimeNanoseconds - measure.startTime.uptimeNanoseconds
      let timeInterval = Double(nanoTime) / 1_000_000_000
      
      print()
      print("Measure (\(measure.name)) is elapsed time: \(timeInterval)")
      print()
   }
   
   class func block(name: String = "", _ block: () -> Void) {
      
      let start = DispatchTime.now()
      
      block()
      
      printResult(measure: TimeMeasureData(startTime: start, name: name))
   }
   
   class func start(name: String = "") {
      
      let measure = TimeMeasure.measures.first { $0.name == name }
      
      if let measure = measure {
         
         print("Measure already running with name: \(measure.name)")
         return
         
      } else {
         
         let newMeasure = TimeMeasureData(startTime: DispatchTime.now(), name: name)
         measures.append(newMeasure)
      }
   }
   
   class func stop(name: String = "") {
      
      let measure = TimeMeasure.measures.first { $0.name == name }
      
      guard measure != nil else {
         print("Measure not found with name: \(name)")
         return
      }
      
      printResult(measure: measure!)
      TimeMeasure.measures.remove(element: measure!)
   }
}
