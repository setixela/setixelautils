//
//  SwiftOverlaysExt.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 22.03.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

// For convenience methods
public extension UIViewController {
   func showOverlay(inView: UIView) {
      SwiftOverlays.showCenteredWaitOverlay(inView)
   }
   
   /**
    Shows wait overlay with activity indicator, centered in the view controller's main view
    
    Do not use this method for **UITableViewController** or **UICollectionViewController**
    
    - returns: Created overlay
    */
   
   func showBlockingWaitOverlay() {
      UIView.animate(withDuration: 0.5) {
         if self.navigationController != nil {
            self.navigationController?.view.alpha = CGFloat(SwiftOverlays.backAlpha)
         } else {
            self.view.alpha = CGFloat(SwiftOverlays.backAlpha)
         }
      }
      SwiftOverlays.showBlockingWaitOverlay()
   }
   
   @discardableResult
   func showWaitOverlay(isNeedDarkBack: Bool = true) -> UIView {
      if isNeedDarkBack {
         UIView.animate(withDuration: 0.5) {
            if self.navigationController != nil {
               self.navigationController?.view.alpha = CGFloat(SwiftOverlays.backAlpha)
            } else {
               self.view.alpha = CGFloat(SwiftOverlays.backAlpha)
            }
         }
      }
      return SwiftOverlays.showCenteredWaitOverlay(self.view)
   }
   
   /**
    Shows wait overlay with activity indicator *and text*, centered in the view controller's main view
    
    Do not use this method for **UITableViewController** or **UICollectionViewController**
    
    - parameter text: Text to be shown on overlay
    
    - returns: Created overlay
    */
   @discardableResult
   func showWaitOverlayWithText(_ text: String) -> UIView {
      UIView.animate(withDuration: 0.5) {
         if self.navigationController != nil {
            self.navigationController?.view.alpha = CGFloat(SwiftOverlays.backAlpha)
         } else {
            self.view.alpha = CGFloat(SwiftOverlays.backAlpha)
         }
      }
      
      return SwiftOverlays.showCenteredWaitOverlayWithText(self.view, text: text)
   }
   
   /**
    Shows *text-only* overlay, centered in the view controller's main view
    
    Do not use this method for **UITableViewController** or **UICollectionViewController**
    
    - parameter text: Text to be shown on overlay
    
    - returns: Created overlay
    */
   @discardableResult
   func showTextOverlay(_ text: String) -> UIView {
      return SwiftOverlays.showTextOverlay(self.view, text: text)
   }
   
   /**
    Shows overlay with text and progress bar, centered in the view controller's main view
    
    Do not use this method for **UITableViewController** or **UICollectionViewController**
    
    - parameter text: Text to be shown on overlay
    
    - returns: Created overlay
    */
   @discardableResult
   func showProgressOverlay(_ text: String) -> UIView {
      return SwiftOverlays.showProgressOverlay(self.view, text: text)
   }
   
   /**
    Shows overlay *with image and text*, centered in the view controller's main view
    
    Do not use this method for **UITableViewController** or **UICollectionViewController**
    
    - parameter image: Image to be added to overlay
    - parameter text: Text to be shown on overlay
    
    - returns: Created overlay
    */
   @discardableResult
   func showImageAndTextOverlay(_ image: UIImage, text: String) -> UIView {
      return SwiftOverlays.showImageAndTextOverlay(self.view, image: image, text: text)
   }
   
   /**
    Shows notification on top of the status bar, similar to native local or remote notifications
    
    - parameter notificationView: View that will be shown as notification
    - parameter duration: Amount of time until notification disappears
    - parameter animated: Should appearing be animated
    */
   
   class func showNotificationOnTopOfStatusBar(_ notificationView: UIView, duration: TimeInterval, animated: Bool = true) {
      SwiftOverlays.showAnnoyingNotificationOnTopOfStatusBar(notificationView, duration: duration, animated: animated)
   }
   
   /**
    Removes all overlays from view controller's main view
    */
   func removeAllOverlays() {
      UIView.animate(withDuration: 0.5) {
         if self.navigationController != nil {
            self.navigationController?.view.alpha = 1
         } else {
            self.view.alpha = 1
         }
      }
      SwiftOverlays.removeAllOverlaysFromView(self.view)
      SwiftOverlays.removeAllBlockingOverlays()
   }
   
   /**
    Updates text on the current overlay.
    Does nothing if no overlay is present.
    
    - parameter text: Text to set
    */
   func updateOverlayText(_ text: String) {
      SwiftOverlays.updateOverlayText(self.view, text: text)
   }
   
   /**
    Updates progress on the current overlay.
    Does nothing if no overlay is present.
    
    - parameter progress: Progress to set 0.0 .. 1.0
    */
   func updateOverlayProgress(_ progress: Float) {
      SwiftOverlays.updateOverlayProgress(self.view, progress: progress)
   }
}
