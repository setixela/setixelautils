//
//  DelegatedCall.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 24.04.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public struct DelegatedCall<Input> {
  
   private(set) var callback: ((Input) -> Void)?
   
   public mutating func delegate<Object: AnyObject>(to object: Object, with callback: @escaping(Object, Input) -> Void) {
      self.callback = { [weak object] input in
         guard let object = object else { return }
         callback(object, input)
      }
   }
}
