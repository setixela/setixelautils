//
//  ArrayExtensions.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 21.03.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public extension Array where Element: Equatable {
   
   // Remove first collection element that is equal to the given `object`:
   mutating func remove(element: Element) {
      if let index = index(of: element) {
         remove(at: index)
      }
   }
}

public extension Array where Element: AnyObject {
   mutating func remove(object: Element) {
      if let index = index(where: { $0 === object }) {
         remove(at: index)
      }
   }
}
