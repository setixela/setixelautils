//
//  LinkedList.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 12/11/2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public class LinkedListNode<T> {
   var value: T
   var next: LinkedListNode?
   weak var previous: LinkedListNode?
   public init(value: T) {
      self.value = value
   }
}

public class LinkedList<T> {
   public typealias Node = LinkedListNode<T>
   
   private var head: Node?
   
   public var isEmpty: Bool {
      return head == nil
   }
   
   public var first: Node? {
      return head
   }
   
   public var last: Node? {
      guard var node = head else {
         return nil
      }
      
      while let next = node.next {
         node = next
      }
      
      return node
   }
   
   public func append(value: T) {
      let newNode = Node(value: value)
      if let lastNode = last {
         newNode.previous = lastNode
         lastNode.next = newNode
      } else {
         head = newNode
      }
   }
   
   public var count: Int {
      guard var node = head else {
         return 0
      }
      
      var count = 1
      while let next = node.next {
         node = next
         count += 1
      }
      
      return count
   }
   
   public func node(atIndex index: Int) -> Node {
      if index == 0 {
         return head!
      } else {
         var node = head!.next
         for _ in 1..<index {
            node = node?.next
            if node == nil {
               break
            }
         }
         return node!
      }
   }
   
   public subscript(index: Int) -> T {
      let node = self.node(atIndex: index)
      return node.value
   }
   
   public func insert(_ value: T, atIndex index: Int) {
      let newNode = Node(value: value)
      if index == 0 {
         newNode.next = head
         head?.previous = newNode
         head = newNode
      } else {
         let prev = node(atIndex: index - 1)
         let next = prev.next
         
         newNode.previous = prev
         newNode.next = prev.next
         prev.next = newNode
         next?.previous = newNode
      }
   }
   
   public func removeAll() {
      head = nil
   }
   
   @discardableResult public func remove(node: Node) -> T {
      let prev = node.previous
      let next = node.next
      
      if let prev = prev {
         prev.next = next
      } else {
         head = next
      }
      
      next?.previous = prev
      node.previous = nil
      node.next = nil
      return node.value
   }
   
   @discardableResult public func removeLast() -> T {
      assert(!isEmpty)
      return remove(node: last!)
   }
   
   @discardableResult public func removeAt(_ index: Int) -> T {
      let node = self.node(atIndex: index)
      return remove(node: node)
   }
   
   public func reverse() {
      var node = head
      // tail = node
      while let currentNode = node {
         node = currentNode.next
         swap(&currentNode.next, &currentNode.previous)
         head = currentNode
      }
   }
   
   public func map<U>(transform: (T) -> U) -> LinkedList<U> {
      let result = LinkedList<U>()
      var node = head
      while node != nil {
         result.append(value: transform(node!.value))
         node = node!.next
      }
      
      return result
   }
   
   public func filter(predicate: (T) -> Bool) -> LinkedList<T> {
      let result = LinkedList<T>()
      var node = head
      while node != nil {
         if predicate(node!.value) {
            result.append(value: node!.value)
         }
         node = node?.next
      }
      return result
   }
}

extension LinkedList: CustomStringConvertible {
   public var description: String {
      var s = "["
      var node = head
      while node != nil {
         s += "\(node!.value)"
         node = node!.next
         if node != nil {
            s += ", "
         }
      }
      return s + "]"
   }
}

public struct LinkedListIndex<T>: Comparable {
   fileprivate let node: LinkedListNode<T>?
   fileprivate let tag: Int
   
   public static func == <T>(lhs: LinkedListIndex<T>, rhs: LinkedListIndex<T>) -> Bool {
      return lhs.tag == rhs.tag
   }
   
   public static func < <T>(lhs: LinkedListIndex<T>, rhs: LinkedListIndex<T>) -> Bool {
      return lhs.tag < rhs.tag
   }
}

extension LinkedList: Collection {
   public func index(after i: LinkedListIndex<T>) -> LinkedListIndex<T> {
      return LinkedListIndex<T>(node: i.node?.next, tag: i.tag + 1)
   }
   
   public subscript(position: LinkedListIndex<T>) -> LinkedListNode<T> {
      return position.node!
   }
   
   public typealias Index = LinkedListIndex<T>
   
   public var startIndex: Index {
      return LinkedListIndex<T>(node: head, tag: 0)
   }
   
   public var endIndex: LinkedListIndex<T> {
      if let head = self.head {
         return LinkedListIndex<T>(node: head, tag: count)
      } else {
         return LinkedListIndex<T>(node: nil, tag: startIndex.tag)
      }
   }
}
